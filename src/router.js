import Vue from "vue";
import Router from "vue-router";
import AppHeader from "./layout/AppHeader";
import AppHeaderTransparent from "./layout/AppHeaderTransparent";
import AppFooter from "./layout/AppFooter";

import Home from "./views/Home.vue";
import Explore from "./views/Explore.vue";
import Upload from "./views/Upload.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  linkExactActiveClass: "active",
  routes: [
    {
      path: "/",
      name: "index",
      components: {
        header: AppHeaderTransparent,
        default: Home
      }
    },
    {
      path: "/upload",
      name: "upload",
      components: {
        header: AppHeaderTransparent,
        default: Upload,
        footer: AppFooter
      }
    },
    {
      path: "/explore",
      name: "explore",
      components: {
        header: AppHeader,
        default: Explore,
        footer: AppFooter
      }
    },
    {
      path: "*",
      redirect: "/"
    }
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
